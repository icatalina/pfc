from django.db import models
from hvad.models import TranslatableModel, TranslatedFields
from django.contrib.postgres.fields import HStoreField

class Category(TranslatableModel):
    class Meta:
        verbose_name_plural='Categories'

    catid = models.CharField(max_length=50, primary_key=True)
    parent_categories = models.ManyToManyField('self', related_name='children_categories', symmetrical=False)

    translations = TranslatedFields(
        title=models.CharField(max_length=255, default=''),
    )

    def __str__(self):
        return "[cat] %s: %s" % (self.catid, self.title)

   # TODO: Parent Category Implementation

class Product(TranslatableModel):
    pid = models.CharField(max_length=50, primary_key=True)
    category = models.ManyToManyField(Category)
    attributes = HStoreField(db_index=True, null=True, blank=True)

    translations = TranslatedFields(
        title=models.CharField(max_length=255, default=''),
        description=models.TextField(default='')
    )

    def __str__(self):
        return "[pro] %s: %s" % (self.pid, self.title)

class Sku(TranslatableModel):
    sku = models.CharField(max_length=50, primary_key=True)
    price = models.DecimalField(max_digits=20, decimal_places=2, default=0.0)
    stock = models.IntegerField(default=0)
    product = models.ForeignKey(Product)
    attributes = HStoreField(db_index=True, null=True, blank=True)

    translations = TranslatedFields(
        description=models.TextField(default='')
    )

    def __str__(self):
        return "[sku] %s" % (self.sku)
