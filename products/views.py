from django.shortcuts import render
from .models import Product, Category
from django.http import Http404
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

# from django.contrib.auth.decorators import login_required
# from django.utils.decorators import method_decorator
#
# def dispatch(self, *args, **kwargs):
    # return super(DetailView, self).dispatch(*args, **kwargs)

class Details(DetailView):
    model = Product
    template_name = 'products/details.html'

    def get_context_data(self, **kwargs):
        context = super(Details, self).get_context_data(**kwargs)
        context['skus'] = context['product'].sku_set.all()
        return context

# def product_details(request, product_id):
    # try:
        # product = Product.objects.language().get(pid=product_id)
    # except Product.DoesNotExist:
        # raise Http404
    # return render(request, 'products/details.html', { 'product': product })

class List(DetailView):
    model = Category
    template_name = 'products/list.html'
    context_object_name = 'list'

    # try:
        # category = Category.objects.language().get(catid=category_id)
    # except Category.DoesNotExist:
        # raise Http404
    # return render(request, 'products/list.html', { 'list': category })
