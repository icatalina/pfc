from django.test import TestCase
from django.utils.html import escape
from unittest import skip
from django.core.urlresolvers import reverse

from products.models import Sku, Product, Category

class ProductDetailsTest(TestCase):
    fixtures = ['functional']
    url = '/product'

    def test_product_details_renders_correct_template(self):
        response = self.client.get(self.url + '/1A')
        self.assertTemplateUsed(response, 'products/details.html')

    def test_not_existing_product_throw_404(self):
        response = self.client.get(self.url + '/this-product-does-not-exist')
        self.assertEqual(response.status_code, 404)

    def test_product_shows_title(self):
        response = self.client.get(self.url + '/1A')
        self.assertContains(response, 'miproducto')

    def test_product_shows_description(self):
        response = self.client.get(self.url + '/1A')
        self.assertContains(response, 'primera descripcion')

    def test_product_skus_are_included_in_request(self):
        response = self.client.get(self.url + '/1A')
        self.assertIn('skus', response.context)

    def test_product_show_sku_attributes(self):
        response = self.client.get(self.url + '/1A')
        self.assertContains(response, 'mycolor / mysize - 20,20€')

class CategoryListTest(TestCase):
    fixtures = ['functional']
    url = '/category'

    def test_category_list_renders_correct_template(self):
        response = self.client.get(self.url + '/my_first_category')
        self.assertTemplateUsed(response, 'products/list.html')

    def test_not_existing_category_throw_404(self):
        response = self.client.get(self.url + '/this_should_show_a_404')
        self.assertEqual(response.status_code, 404)

    def test_category_shows_title(self):
        response = self.client.get(self.url + '/my_first_category')
        self.assertContains(response, 'My First Category')

    def test_products_show_on_the_page(self):
        response = self.client.get(self.url + '/my_first_category')
        self.assertContains(response, 'miproducto')
        self.assertContains(response, 'segundoproducto')
