from django.test import TestCase
from products.preprocessors import categories

class ProductPreprocessorTests(TestCase):
    fixtures = ['unittest']

    def test_should_return_only_categories_under_ROOT(self):
        retrieved_categories = categories(True)
        self.assertIn('CATEGORY_LIST', retrieved_categories)

        retrieved_categories = retrieved_categories['CATEGORY_LIST']
        self.assertTrue(len(retrieved_categories) > 0)

        for cat in retrieved_categories:
            cat.parent_categories.get(catid='ROOT')
