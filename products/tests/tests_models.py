from django.test import TestCase
from products.models import Sku, Product, Category
from sikker.settings import LANGUAGE_CODE
from django.db.utils import IntegrityError
from decimal import Decimal

class SkuTests(TestCase):
    fixtures = ['unittest']

    def test_can_create_a_sku(self):
        sku = Sku.objects.language().create(sku='test_create_sku', description='def-desc', stock=33, product_id='test_product', attributes={'key': 'value', 'key2': 'value2'})
        sku.translate('other-lang')
        sku.description = 'other-desc'

        sku.save()

        sku = Sku.objects.language('other-lang').get(sku='test_create_sku')

        self.assertEqual(sku.sku, 'test_create_sku')
        self.assertEqual(sku.description, 'other-desc')
        self.assertEqual(sku.stock, 33)

        sku = Sku.objects.language().get(pk='test_create_sku')

        self.assertEqual(sku.sku, 'test_create_sku')
        self.assertEqual(sku.description, 'def-desc')
        self.assertEqual(sku.stock, 33)
        self.assertEqual(sku.attributes, {'key': 'value', 'key2': 'value2'})

        self.assertCountEqual(sku.get_available_languages(), [LANGUAGE_CODE, 'other-lang'])

    def test_cant_duplicate_sku_ids(self):
        sku = Sku.objects.language().create(sku='test_duplicates', description='def-desc', stock=33, product_id='test_product')
        sku.save()

        with self.assertRaises(IntegrityError):
            sku = Sku.objects.language().create(sku='test_duplicates', description='other-desc', stock=33, product_id='test_product')

    def test_defaults(self):
        sku = Sku.objects.language().create(sku='testing_defaults', product_id='test_product')
        self.assertEqual(sku.sku, 'testing_defaults')
        self.assertEqual(sku.description, '')
        self.assertEqual(sku.stock, 0)
        self.assertEqual(sku.price, 0.0)
        self.assertEqual(sku.get_available_languages()[0], LANGUAGE_CODE)

    def test_decimal_places_in_price(self):
        sku = Sku.objects.language().create(sku='decimal_places', price=123.123456789, product_id='test_product')
        sku.save()

        sku = Sku.objects.language().get(sku='decimal_places')
        self.assertEqual(sku.price, Decimal('123.12'))

    def test_sku_is_related_to_product(self):
        sku = Sku.objects.create(sku='related_to_product', product_id='test_product')
        prod = Product.objects.get(pid='test_product')

        self.assertIn(sku, prod.sku_set.all())

    def test_multiple_skus_to_one_product(self):
        prod = Product.objects.language().create(pid='prod_multiple', title='My Title')
        sku1 = Sku.objects.language().create(sku='multiple_1', product_id='prod_multiple')
        sku2 = Sku.objects.language().create(sku='multiple_2', product_id='prod_multiple')
        sku3 = Sku.objects.language().create(sku='multiple_3', product_id='test_product')

        prod.save()
        sku1.save()
        sku2.save()
        sku3.save()

        prod = Product.objects.language().get(pid='prod_multiple')

        skus = prod.sku_set.all()

        self.assertEqual(len(skus), 2)
        self.assertCountEqual(skus, [sku1, sku2])

    def test_string_representation(self):
        sku = Sku.objects.create(sku='string_representation', product_id='test_product')
        self.assertEqual(str(sku), '[sku] string_representation')

class ProductTest(TestCase):

    def test_can_create_a_product(self):
        prod = Product.objects.language().create(pid='create_product', title='def-title', description='def-desc', attributes={'key': 'value', 'key2': 'value2'})
        prod.translate('other-lang')
        prod.title = 'other-title'
        prod.description = 'other-desc'

        prod.save()

        self.assertEqual(prod.pid, 'create_product')
        self.assertEqual(prod.title, 'other-title')
        self.assertEqual(prod.description, 'other-desc')

        prod = Product.objects.language(LANGUAGE_CODE).get(pk='create_product')

        self.assertEqual(prod.pid, 'create_product')
        self.assertEqual(prod.title, 'def-title')
        self.assertEqual(prod.description, 'def-desc')
        self.assertEqual(prod.attributes, {'key': 'value', 'key2': 'value2'})

        self.assertCountEqual(prod.get_available_languages(), [LANGUAGE_CODE, 'other-lang'])

    def test_duplicates(self):
        prod = Product.objects.language().create(pid='duplicates', title='def-title', description='def-desc')

        with self.assertRaises(IntegrityError):
            prod = Product.objects.language().create(pid='duplicates', title='def-title', description='def-desc')

    def test_string_representation(self):
        prod = Product.objects.create(pid='string_representation', title='string representation', description='def-desc')
        self.assertEqual(str(prod), '[pro] string_representation: string representation')

    def test_product_can_be_in_multiple_categories(self):
        cat1 = Category.objects.create(catid='prod_mult_cat_1')
        cat2 = Category.objects.create(catid='prod_mult_cat_2')
        prod = Product.objects.create(pid='multiple_categories')

        cat1.product_set.add(prod)
        cat2.product_set.add(prod)

        cat1.save()
        cat2.save()
        prod.save()

        cat1 = Category.objects.get(catid='prod_mult_cat_1')
        cat2 = Category.objects.get(catid='prod_mult_cat_2')

        self.assertIn(prod, cat1.product_set.all())
        self.assertIn(prod, cat2.product_set.all())

class CategoryTest(TestCase):

    def test_can_create_a_category(self):
        cat = Category.objects.language().create(catid='create_category', title='def-title')

        cat.translate('other-lang')
        cat.title = 'other-title'

        cat.save()

        cat = Category.objects.language().get(catid='create_category')
        self.assertEqual(cat.catid, 'create_category')
        self.assertEqual(cat.title, 'def-title')

        cat = Category.objects.language('other-lang').get(catid='create_category')
        self.assertEqual(cat.catid, 'create_category')
        self.assertEqual(cat.title, 'other-title')

        self.assertCountEqual(cat.get_available_languages(), [LANGUAGE_CODE, 'other-lang'])

    def test_ROOT_category_should_exist(self):
        cat = Category.objects.get(catid='ROOT')
        self.assertTrue(cat.catid == 'ROOT')
        self.assertEqual(len(cat.parent_categories.all()), 0)

    def test_can_create_a_children_for_a_category(self):
        cat = Category.objects.language().create(catid='create_category')
        cat.children_categories.add('ROOT')
        cat.save()
        root = Category.objects.get(catid='ROOT')
        self.assertEqual(root.parent_categories.first(), cat)

    def test_can_create_a_parent_for_a_category(self):
        cat = Category.objects.language().create(catid='create_category')
        cat.parent_categories.add('ROOT')
        cat.save()
        root = Category.objects.get(catid='ROOT')
        self.assertEqual(root.children_categories.first(), cat)

    def test_can_create_more_than_one_parent_for_a_category(self):
        cat1 = Category.objects.language().create(catid='create_category')
        cat2 = Category.objects.language().create(catid='create_2_category')
        cat1.parent_categories.add(cat2)
        cat1.parent_categories.add('ROOT')
        cat1.save()

        root = Category.objects.get(catid='ROOT')
        self.assertEqual(len(cat1.parent_categories.all()), 2)
        self.assertIn(root, cat1.parent_categories.all())
        self.assertIn(cat2, cat1.parent_categories.all())

        self.assertEqual(cat1, cat2.children_categories.first())
        self.assertEqual(cat1, root.children_categories.first())

    def test_can_create_more_than_one_children_for_a_category(self):
        cat1 = Category.objects.language().create(catid='create_category')
        cat2 = Category.objects.language().create(catid='create_2_category')
        cat1.children_categories.add(cat2)
        cat1.children_categories.add('ROOT')
        cat1.save()

        root = Category.objects.get(catid='ROOT')
        self.assertEqual(len(cat1.children_categories.all()), 2)
        self.assertIn(root, cat1.children_categories.all())
        self.assertIn(cat2, cat1.children_categories.all())

        self.assertEqual(cat1, cat2.parent_categories.first())
        self.assertEqual(cat1, root.parent_categories.first())

    def test_duplicates(self):
        Category.objects.language().create(catid='duplicates')

        with self.assertRaises(IntegrityError):
            Category.objects.language().create(catid='duplicates')

    def test_category_contains_multiple_products(self):
        cat = Category.objects.create(catid='multiple_products')

        prod1 = Product.objects.create(pid='1234')
        prod2 = Product.objects.create(pid='12345')

        cat.product_set.add(prod1)
        cat.product_set.add(prod2)

        cat.save()

        cat = Category.objects.get(catid='multiple_products')
        prods = cat.product_set.all()

        self.assertEqual(len(prods), 2)
        self.assertCountEqual(prods, [prod1, prod2])

    def test_string_representation(self):
        cat = Category.objects.create(catid='string_representation', title='string representation')
        self.assertEqual(str(cat), '[cat] string_representation: string representation')
