from django.contrib import admin
from hvad.admin import TranslatableAdmin
import products.models

class ProductAdmin(TranslatableAdmin):
    pass

class SkuAdmin(TranslatableAdmin):
    pass

class CategoryAdmin(TranslatableAdmin):
    pass

admin.site.register(products.models.Product, ProductAdmin)
admin.site.register(products.models.Category, CategoryAdmin)
admin.site.register(products.models.Sku, SkuAdmin)

# Register your models here.
