from django.conf.urls import include, url

from .. import views

urlpatterns = [
    # Examples:
    # url(r'^$', 'sikker.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^(?P<pk>[\w\d-]+)$', views.Details.as_view(), name="details"),
]
