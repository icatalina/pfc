from products.models import Category

def categories(request):
    return { 'CATEGORY_LIST': Category.objects.language().filter(parent_categories__catid='ROOT') }
