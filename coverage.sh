#!/usr/bin/env bash
coverage run --omit="manage.py,*migrations*,*test*,*__init__.py,*wsgi.py" --source='.' manage.py test $1
rm -Rf htmlcov
coverage html
open htmlcov/index.html


