from django import template

register = template.Library()

@register.filter(name='values_sorted_by_key')
def values_sorted_by_key(value):
    data = []
    if isinstance(value, dict):
        for key in sorted(value.keys(), key=lambda s: s.lower()):
            data.append(value[key])

    return data

@register.filter(is_safe=True)
def label_with_classes(value, arg):

    return value.label_tag(attrs={'class': arg})

@register.filter(is_safe=True)
def field_with_classes(value, arg):

    return value.as_widget(attrs={'class': arg})

@register.filter(is_safe=True)
def filter_dict(value, args):
    data = []
    args = args.split(',')

    if isinstance(value, dict):
        for key in sorted(args, key=lambda s: s.lower()):
            data.append(value[key])

    return data

@register.filter(is_safe=True)
def image(product, classes, size = '', variant=1):
    if isinstance(product.attributes, dict) and 'image' in product.attributes:
        return '<img src="/static/images/%s%s_1_1_%s.jpg" class="%s">' % (size, product.attributes['image'], variant, classes)
    return ''

@register.filter(is_safe=True)
def lgprimage(product, classes):
    return image(product, classes)

@register.filter(is_safe=True)
def smprimage(product, classes):
    return image(product, classes, size='thumbs/')
