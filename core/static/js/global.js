'use strict';

$('.submit-on-change').on('change', function() {
  $(this).closest('form').submit();
});

$('.product .thumbnail').hover(
  function() {
    var $this = $('.product__image-wrap', this),
      $altImage = $this.find('.product__alt-image');

    if (!$altImage.length) {
      $altImage = $('<img src="' + $this.find('img').attr('src').replace('1_1_1.', '2_1_1.') + '" class="img-responsive product__alt-image">');
      $this.append($altImage);
      $altImage.hide();
    }

    $altImage.fadeIn();

  },
  function() {
    $('.product__alt-image', this).fadeOut();
  }
);
