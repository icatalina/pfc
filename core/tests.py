from django.test import TestCase
from .templatetags.lib import values_sorted_by_key, lgprimage, smprimage

class ValuesSortedTest(TestCase):

    def test_should_return_the_values_of_a_dict_sorted_by_key_case_insensitive(self):
        data = {
            'CCCC': 'ccc-value',
            'eeee': 'eee-value',
            'AAAA': 'aaa-value',
            'bbbb': 'bbb-value',
            'dddd': 'ddd-value'
        }
        result = values_sorted_by_key(data)
        self.assertEqual(result, ['aaa-value', 'bbb-value', 'ccc-value', 'ddd-value', 'eee-value'])

    def test_should_return_an_empty_array_if_an_empty_object_is_provided(self):
        data = {}
        result = values_sorted_by_key(data)
        self.assertEqual(result, [])

    def test_should_return_an_empty_array_something_else_is_provided(self):
        data = 'helloworld'
        result = values_sorted_by_key(data)
        self.assertEqual(result, [])

class ImageTag(TestCase):

    def test_should_return_the_large_image_tag(self):
        product = type('test', (), {})()
        product.attributes = {
            'image': 'test'
        }
        output = lgprimage(product, 'test-class')
        self.assertEqual(output, '<img src="/static/images/test_1_1_1.jpg" class="test-class">')

    def test_an_empty_object_should_return_an_empty_string(self):
        product = type('test', (), {})()
        product.attributes = {}
        output = lgprimage(product, 'test-class')
        self.assertEqual(output, '')

    def test_should_return_the_small_image_tag(self):
        product = type('test', (), {})()
        product.attributes = {
            'image': 'test2'
        }
        output = smprimage(product, 'test-class')
        self.assertEqual(output, '<img src="/static/images/thumbs/test2_1_1_1.jpg" class="test-class">')
