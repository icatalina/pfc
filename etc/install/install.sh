#!/bin/bash

# Script to set up a Django project on Vagrant.

# Installation settings

VIRTUALENV_NAME='deploy'

PROJECT_DIR=/home/vagrant/deploy
VIRTUALENV_DIR=/home/vagrant/.virtualenvs/deploy
cd $PROJECT_DIR
DB_NAME=`find -type f -name "settings.py" | xargs python3 etc/install/name.py`

PGSQL_VERSION=9.3

locale-gen en_GB.UTF-8
dpkg-reconfigure locales

export LANGUAGE=enGB.UTF-8
export LANG=en_GB.UTF-8
export LC_ALL=en_GB.UTF-8

apt-get update -y
apt-get install -y build-essential python3 python3-dev python3-pip

if ! command -v psql; then
    apt-get install -y postgresql-$PGSQL_VERSION postgresql-contrib-$PGSQL_VERSION libpq-dev
    cp $PROJECT_DIR/etc/install/pg_hba.conf /etc/postgresql/$PGSQL_VERSION/main/
    /etc/init.d/postgresql reload
fi

if [[ ! -f /usr/local/bin/virtualenv ]]; then
    pip3 install virtualenv
fi

echo "source $PROJECT_DIR/etc/install/bashrc" >> /home/vagrant/.bashrc

psql -Upostgres --command="create user $DB_NAME password '$DB_NAME' SUPERUSER CREATEROLE;"
psql -Upostgres template1 -c 'create extension if not exists hstore;'

createdb -U$DB_NAME $DB_NAME

psql $DB_NAME -U$DB_NAME --command="CREATE EXTENSION hstore;"

virtualenv -p $(which python3) virtualenv

source $PROJECT_DIR/virtualenv/bin/activate
pip3 install -r requirements.txt

chmod a+x $PROJECT_DIR/manage.py
python3 ./manage.py migrate
