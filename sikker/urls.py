from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from . import views
from products.urls import details as details
from products.urls import list as list
from customers import urls as customers
admin.site.site_header = 'Sikker'
urlpatterns = [
    # Examples:
    # url(r'^$', 'sikker.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.home_page, name='home'),
    url(r'^product/', include(details, namespace='product')),
    url(r'^category/', include(list, namespace='category')),
    url(r'^account/', include(customers, namespace='account')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
