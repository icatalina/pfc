from django.shortcuts import render
from products.models import Product
from .settings import CONFIG

def home_page(request):
    products = Product.objects.language().all().order_by('?')[:4]
    other = Product.objects.language().all().order_by('?')[:4]
    return render(request, 'base/home.html', {'config': CONFIG, 'hero_products': products, 'hero_products_2': other })
