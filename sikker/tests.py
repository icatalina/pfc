from django.test import TestCase
from .settings import CONFIG

class HomePageTest(TestCase):

    def test_the_home_page_renders_the_correct_template(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'base/home.html')

    def test_title_set_in_config(self):
        assert 'title' in CONFIG

    def test_title_in_the_home_page(self):
        response = self.client.get('/')
        self.assertContains(response, CONFIG['title'])
