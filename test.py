#!/usr/bin/env python
import time
import sys
from subprocess import call
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler

run = ['python3', 'manage.py', 'test']


class Tester(PatternMatchingEventHandler):
    patterns = ['*.py', '*.html', '*.json', '*.yaml']

    def on_created(self, event):
        call(['clear'])
        call(run)


if __name__ == "__main__":
    args = []

    # Makes it possible to use paths instead of having to specify dots
    for value in sys.argv[1:]:
        args.append(value.replace('/', '.').replace('.py:', '.').replace('.py', ''))

    print(args)
    run.extend(args)
    event_handler = Tester()
    observer = Observer()
    observer.schedule(event_handler, path='.', recursive=True)
    call(['clear'])
    call(run)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
