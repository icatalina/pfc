from .base import FunctionalTest
from time import sleep

from django.core.urlresolvers import reverse

class CategoryTests(FunctionalTest):
    fixtures = ['functional']

    def test_should_show_a_list_of_products_in_certain_category(self):
        self.browser.get(self.server_url + '/category/my_first_category')
        products = self.browser.find_elements_by_class_name('product')
        self.assertIn('MIPRODUCTO', products[0].text)
        self.assertIn('SEGUNDOPRODUCTO', products[1].text)


    def test_should_render_a_404_if_category_doesnt_exists(self):
        self.browser.get(self.server_url + '/category/no_category_has_this_name')
        body = self.browser.find_element_by_tag_name('body').text
        self.assertIn('Not Found', body)

