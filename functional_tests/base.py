from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.core.urlresolvers import reverse

from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains

import sys
from time import sleep

class Elements():
    def header__login(self, element):
        return element.find_element_by_css_selector('.header__login > a')
    def logo(self, element):
        return element.find_element_by_css_selector('.header__logo > a > img')

class FunctionalTest(StaticLiveServerTestCase):


    @classmethod
    def setUpClass(cls):
        for arg in sys.argv:
            if 'liveserver' in arg:
                cls.server_url = 'http://' + arg.split('=')[1]
                return

        super().setUpClass()
        cls.server_url = cls.live_server_url

    @classmethod
    def tearDownClass(cls):
        if cls.server_url == cls.live_server_url:
            super().tearDownClass()

    def setUp(self):
        self.elements = Elements()
        self.browser = webdriver.Firefox()
        self.reverse('home')
        form = self.get_form('language__form')
        Select(form['language']).select_by_value('es')

    def tearDown(self):
        self.browser.quit()

    def get_form(self, form_name):
        exports = {
            '__form_name': form_name,
        }

        exports['__form_wrap'] = self.browser.find_element_by_class_name(exports['__form_name'])
        exports['__form'] = exports['__form_wrap'].find_element_by_tag_name('form')

        elements = exports['__form'].find_elements_by_tag_name('input')
        elements += exports['__form'].find_elements_by_tag_name('textarea')
        elements += exports['__form'].find_elements_by_tag_name('select')

        exports['__count'] = len(elements)

        for elem in elements:
            exports[elem.get_attribute('name')] = elem

        return exports

    def reverse(self, *args, **kwargs):
        self.browser.get(self.get_url(*args, **kwargs))

    def get_url(self, *args, **kwargs):
        return self.server_url + reverse(*args, **kwargs)

    def css(self, selector, element=False):
        if (element):
            return element.find_elements_by_css_selector(selector)
        else:
            return self.browser.find_elements_by_css_selector(selector)

    def sleep(self, seconds):
        sleep(seconds)

    def hover(self, element):
        hov = ActionChains(self.browser).move_to_element(element)
        hov.perform()

    def assertLenEqual(self, list, length):
        self.assertEqual(len(list), length)
