from .base import FunctionalTest
from selenium.webdriver.support.ui import WebDriverWait
from sikker.settings import CONFIG

class BasicServerTest(FunctionalTest):
    def test_that_the_server_is_running(self):
        # Edith goes to the home page
        self.browser.get(self.server_url)
        self.assertIn(CONFIG['title'], self.browser.title)

