from .base import FunctionalTest
from selenium.webdriver.support.ui import Select
from unittest import skip

class HeaderTests(FunctionalTest):
    fixtures = ['functional']

    def test_have_the_logo_within_the_header(self):
        # The logo is contain within the header
        self.browser.get(self.server_url + '/product/1A')
        logo = self.elements.logo(self.browser)
        self.assertEqual(logo.get_attribute('src').endswith('.png'), True);

        #Clicking the logo, takes us to the homepage
        logo.click()
        self.assertEqual(self.browser.current_url, self.server_url + '/')


    def test_should_show_the_minibasket_with_quantity(self):
        self.browser.get(self.server_url)
        minibasket = self.css('.header__minibasket__link')[0]
        self.assertTrue('0' in minibasket.text)

        self.browser.get(self.server_url + '/product/1A')
        form = self.get_form('product__add-basket')
        form['submit'].click()

        self.elements.logo(self.browser).click()

        minibasket = self.css('.header__minibasket__link')[0]
        self.assertTrue('1' in minibasket.text)

        self.hover(self.css('.header__minibasket')[0])
        minibasket = self.css('.minibasket')[0]
        self.assertTrue('miproducto' in minibasket.text)

        self.browser.get(self.server_url + '/product/2A')
        form = self.get_form('product__add-basket')
        Select(form['quantity']).select_by_value('3')
        form['submit'].click()

        minibasket = self.css('.header__minibasket__link')[0]
        self.assertTrue('4' in minibasket.text)
        self.hover(self.css('.header__minibasket')[0])
        minibasket = self.css('.minibasket')[0]
        self.assertTrue('miproducto' in minibasket.text)
        self.assertTrue('segundoproducto' in minibasket.text)
        self.assertTrue('segundoproducto' in minibasket.text)

        total = self.css('.minibasket-total')[0]
        self.assertTrue('50,80' in total.text)

    def test_should_show_the_minibasket_with_sku_attributes(self):
        self.browser.get(self.server_url)
        minibasket = self.css('.header__minibasket__link')[0]

        self.browser.get(self.server_url + '/product/1A')
        form = self.get_form('product__add-basket')
        form['submit'].click()

        self.elements.logo(self.browser).click()

        minibasket = self.css('.minibasket')[0]

        self.browser.get(self.server_url + '/product/2A')
        form = self.get_form('product__add-basket')
        Select(form['quantity']).select_by_value('3')
        form['submit'].click()

        minibasket = self.css('.minibasket')[0]
        sku_attributes = [
            'mycolor, mysize'
        ]

        self.hover(self.css('.header__minibasket')[0])
        skus = self.css('.sku_attributes', minibasket)
        self.assertEqual(len(skus), len(sku_attributes))
        for index, sku in enumerate(skus):
            self.assertIn(sku_attributes[index], sku.text)



    def test_hierarchy_of_categories_should_be_ok(self):
        menu_items = ['my_first_category']
        submenu_items = [
            ['my_first_child_category', 'my_second_child_category']
        ]

        menu = self.css('.navbar-nav > li', self.browser)
        self.assertEqual(len(menu), len(menu_items))

        for index, item in enumerate(menu):
            self.hover(item)
            submenus = self.css('ul > li', item)

            self.assertIn(menu_items[index], item.get_attribute('innerHTML'))
            self.assertEqual(len(submenus), len(submenu_items[index]))


            for subindex, subitem in enumerate(submenus):
                self.assertIn(submenu_items[index][subindex], subitem.get_attribute('innerHTML'))



        # It should show only categories under ROOT
        # It Should show subcategories under the right category

    # Disabled for now
    # def test_should_show_the_language_choices_and_change_the_language_on_click(self):
    #   self.browser.get(self.server_url)
