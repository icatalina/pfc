from .base import FunctionalTest
from time import sleep

from django.core.urlresolvers import reverse
from selenium.webdriver.support.ui import Select

class ProductsTests(FunctionalTest):
    fixtures = ['functional']

    def test_should_show_the_product_details_page(self):
        self.browser.get(self.server_url + '/product/1A')
        title = self.browser.find_element_by_class_name('product__title').text
        description = self.browser.find_element_by_class_name('product__description').text
        self.assertIn('miproducto', title)
        self.assertIn('primera descripcion', description)

    def test_should_show_a_404(self):
        self.browser.get(self.server_url + '/product/this_should_show_a_404')
        body = self.browser.find_element_by_tag_name('body').text
        self.assertIn('Not Found', body)

    def test_title_should_include_product_title(self):
        self.browser.get(self.server_url + '/product/1A')
        self.assertIn('miproducto', self.browser.title)

    def test_the_product_should_have_a_add_to_cart_button(self):
        # Add to Cart button should be present on PDP
        self.browser.get(self.server_url + '/product/1A')
        add_to_cart = self.browser.find_element_by_class_name('product__add-to-cart')

        # When click, the user should be redirected to the basket page
        add_to_cart.click()
        self.assertEqual(self.browser.current_url, self.server_url + reverse('account:basket'))

    def test_should_show_the_right_number_of_skus_and_the_sku_attributes_on_the_dropdown(self):
        self.browser.get(self.server_url + '/product/1A')
        form = self.get_form('product__add-basket')
        s_form = Select(form['sku'])
        s_form.select_by_value('12339')
        elem = s_form.first_selected_option

        self.assertEqual(len(self.css('option', form['sku'])), 2)
        self.assertTrue('mycolor / mysize - 20,20€' in elem.text)
