from .base import FunctionalTest

from django.core.urlresolvers import reverse
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import Select

from selenium.webdriver.common.action_chains import ActionChains


from time import sleep

class LoginPageTests(FunctionalTest):
    fixtures = ['users']

    def setUp(self):
        if not hasattr(self, 'browser'):
            super().setUp()

        self.browser.get(self.server_url)
        login = self.elements.header__login(self.browser)
        login.click()

    def test_rhs_login_button_should_take_you_to_the_login_page(self):
        self.assertEqual(self.browser.current_url, self.server_url + reverse('account:login'))

    def test_should_be_able_to_login_using_from_login_page(self):
        # Should show the login form
        form = self.get_form('login__form')

        self.assertEqual(form['username'].tag_name, 'input')
        self.assertEqual(form['password'].tag_name, 'input')
        self.assertEqual(form['login'].tag_name, 'input')

        # Should enter a wrong username and password
        form['username'].send_keys('test_user_wrong_username')
        form['password'].send_keys('testpassword_wrong_password')

        # After clicking Login, it should return to the login page, throwing an error
        form['login'].click()
        self.assertEqual(self.browser.current_url, self.server_url + reverse('account:login'))

        error_message = self.css('.has-error')
        self.assertEqual(len(error_message), 1)
        self.assertTrue('invalid' in error_message[0].text)

        # Should enter the username and password
        form = self.get_form('login__form')

        # Should keep username but not password
        self.assertEqual(form['username'].get_attribute('value'), 'test_user_wrong_username')
        self.assertEqual(form['password'].get_attribute('value'), '')

        # Should enter the right username and password
        form['username'].clear()
        form['username'].send_keys('test_user')
        form['password'].send_keys('testpassword')

        # After clicking Login, it should show the login page
        form['login'].click()
        self.assertTrue('My Account' in self.browser.title)
        self.assertEqual(self.browser.current_url, self.server_url + reverse('account:myaccount'))


    def test_shold_be_able_to_create_account_from_login_page(self):
        # Form and the related elements are presents on the page
        form = self.get_form('signup__form')
        self.assertEqual(form['username'].tag_name, 'input')
        self.assertEqual(form['password'].tag_name, 'input')
        self.assertEqual(form['confirm_password'].tag_name, 'input')
        self.assertEqual(form['signup'].tag_name, 'input')

        # Existing User
        form['username'].send_keys('test_user')
        form['password'].send_keys('test_password')
        form['confirm_password'].send_keys('test_password')
        form['email'].send_keys('test@email.com')
        form['signup'].click()

        self.assertEqual(self.browser.current_url, self.server_url + reverse('account:login'))
        error_message = self.css('.signup__form .has-error')
        self.assertEqual(len(error_message), 1)
        self.assertTrue('usuario' in error_message[0].text)

        # Password Confirmation
        self.setUp()

        form = self.get_form('signup__form')
        form['username'].send_keys('test_user_new')
        form['password'].send_keys('test_password')
        form['confirm_password'].send_keys('test_password_wrong_password')
        form['email'].send_keys('test@email.com')
        form['signup'].click()

        self.assertEqual(self.browser.current_url, self.server_url + reverse('account:login'))
        error_message = self.css('.signup__form .has-error')
        self.assertEqual(len(error_message), 1)
        self.assertTrue('match' in error_message[0].text)

        # Should Register and account and jump to My Account
        self.setUp()

        form = self.get_form('signup__form')
        form['username'].send_keys('test_user_new')
        form['password'].send_keys('test_password')
        form['confirm_password'].send_keys('test_password')
        form['email'].send_keys('test@email.com')
        form['signup'].click()

        self.assertEqual(self.browser.current_url, self.server_url + reverse('account:myaccount'))

class MyAccountPageTests(FunctionalTest):
    fixtures = ['users']

    def setUp(self):
        if not hasattr(self, 'browser'):
            super().setUp()

        self.browser.get(self.server_url)
        login = self.elements.header__login(self.browser)
        login.click()

        # Should start everytest from my account
        form = self.get_form('login__form')
        form['username'].send_keys('test_user')
        form['password'].send_keys('testpassword')
        form['login'].click()

    def test_should_render_the_right_template_when_logged_in(self):
        self.assertTrue('My Account' in self.browser.title)
        self.assertEqual(self.browser.current_url, self.server_url + reverse('account:myaccount'))

    def test_should_redirect_to_login_when_not_logged_in(self):
        self.browser.delete_all_cookies()
        self.browser.get(self.browser.current_url)
        self.assertEqual(self.browser.current_url, self.server_url + reverse('account:login').rstrip('/') + '?next=' + reverse('account:myaccount'))

    def test_should_be_able_to_edit_an_user_from_my_account(self):
        form = self.get_form('myaccount__form')

        self.assertEqual(form['username'].tag_name, 'input')
        self.assertEqual(form['email'].tag_name, 'input')
        self.assertEqual(form['password'].tag_name, 'input')
        self.assertEqual(form['confirm_password'].tag_name, 'input')
        self.assertEqual(form['update'].tag_name, 'input')

        self.assertEqual(form['username'].get_attribute('value'), 'test_user')
        self.assertEqual(form['email'].get_attribute('value'), 'test@emailaddress.com')
        self.assertEqual(form['password'].get_attribute('value'), '')
        self.assertEqual(form['confirm_password'].get_attribute('value'), '')

        # Should be able to update only the user and email without changing the password
        form['username'].clear()
        form['email'].clear()

        form['username'].send_keys('newusername')
        form['email'].send_keys('test@newemail.com')

        form['update'].click()


        # Should let you login with the new username and old password

        self.hover(self.css('.header__login')[0])
        self.css('.account__logout-button')[0].click()
        #self.browser.find_element_by_class_name('account__logout-button').click()
        self.browser.get(self.server_url)

        login = self.elements.header__login(self.browser)
        login.click()

        form = self.get_form('login__form')
        form['username'].send_keys('newusername')
        form['password'].send_keys('testpassword')
        form['login'].click()

        form = self.get_form('myaccount__form')

        self.assertEqual(form['username'].get_attribute('value'), 'newusername')
        self.assertEqual(form['email'].get_attribute('value'), 'test@newemail.com')

        self.assertEqual(self.browser.current_url, self.server_url + reverse('account:myaccount'))

        # Should fail to update when the passwords don't match
        form['password'].send_keys('newpassword')
        form['confirm_password'].send_keys('wrong_password')
        form['update'].click()

        error_message = self.css('.myaccount__form .has-error')
        self.assertLenEqual(error_message, 1)
        self.assertTrue('match' in error_message[0].text)

        # Should change the password when both match
        form = self.get_form('myaccount__form')
        form['password'].send_keys('newpassword')
        form['confirm_password'].send_keys('newpassword')
        form['update'].click()

        # Should be able to login with the new password
        self.hover(self.css('.header__login')[0])
        self.css('.account__logout-button')[0].click()
        self.browser.get(self.server_url)

        login = self.elements.header__login(self.browser)
        login.click()

        form = self.get_form('login__form')
        form['username'].send_keys('newusername')
        form['password'].send_keys('newpassword')
        form['login'].click()

        self.assertEqual(self.browser.current_url, self.server_url + reverse('account:myaccount'))

        form = self.get_form('myaccount__form')
        self.assertEqual(form['username'].get_attribute('value'), 'newusername')
        self.assertEqual(form['email'].get_attribute('value'), 'test@newemail.com')

class BasketPageTest(FunctionalTest):
    fixtures = ['functional']

    def test_basket_adding_products_to_the_basket(self):
        # First time we access the basket, an empty list is found
        self.reverse('account:basket')
        self.browser.find_element_by_class_name('basket__empty-list')
        with self.assertRaises(NoSuchElementException):
            self.browser.find_element_by_class_name('basket__product-list')

        # We navigate to a product page and add the product to the basket
        self.reverse('product:details', kwargs={'pk':'1A'})
        form = self.get_form('product__add-basket')
        Select(form['sku']).select_by_value('12339')
        Select(form['quantity']).select_by_value('3')
        form['submit'].click()

        # The basket should show now the product and information about it
        self.assertEqual(self.browser.current_url, self.get_url('account:basket'))

        product_list = self.browser.find_element_by_class_name('basket__product-list')
        with self.assertRaises(NoSuchElementException):
            self.browser.find_element_by_class_name('basket__empty-list')

        sku = product_list.find_element_by_class_name('product__sku')
        quantity = product_list.find_element_by_class_name('product__quantity')
        self.assertTrue('miproducto' in product_list.text)
        self.assertTrue('(Sku: 12339)' in sku.text)
        self.assertTrue('3' in quantity.text)

        self.browser.find_element_by_css_selector('.product__delete.sku_12339').click()
        self.assertEqual(self.browser.current_url, self.get_url('account:basket'))

        with self.assertRaises(NoSuchElementException):
            self.browser.find_element_by_class_name('product__sku')


    def test_should_show_sku_attributes_on_the_basket_page(self):
        # We navigate to product pages and some products to the basket
        self.reverse('product:details', kwargs={'pk':'1A'})
        form = self.get_form('product__add-basket')
        Select(form['sku']).select_by_value('12339')
        Select(form['quantity']).select_by_value('3')
        form['submit'].click()

        self.reverse('product:details', kwargs={'pk':'2A'})
        form = self.get_form('product__add-basket')
        form['submit'].click()

        sku_attributes = [
            'mycolor, mysize'
        ]

        products = self.css('.basket__product-list')[0]
        skus = self.css('.product__sku-attributes', products)

        self.assertEqual(len(skus), len(sku_attributes))

        for index, sku in enumerate(skus):
            self.assertIn(sku_attributes[index], sku.text)
