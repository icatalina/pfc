from django.test import TestCase
from django.contrib.auth.models import User
from customers.forms import SignupForm

from django.test.client import RequestFactory
from unittest import skip
import mock

class SignUpFormTest(TestCase):

    @mock.patch('customers.forms.login')
    def test_form_create_user_and_login(self, mock_login):
        form = SignupForm(data = {
            'username': 'testuser',
            'password': 'testpassword',
            'confirm_password': 'testpassword',
            'email': 'user@email.com'
        })

        self.assertEqual(form.is_valid(RequestFactory()), True)
        self.assertTrue(mock_login.called)


    def test_form_no_twins(self):
        user = User.objects.create_user('test_user', 'test_user@test.com', 'testpassword')
        user.save()

        form = SignupForm(data = {
            'username': 'test_user',
            'password': 'testpassword',
            'confirm_password': 'testpassword',
            'email': 'test_user@test.com'
        })

        self.assertFalse(form.is_valid(RequestFactory()))
        self.assertTrue(len(form.errors), 1)
        self.assertTrue(len(form.errors['username']), 1)


    @mock.patch('customers.forms.login')
    def test_form_save(self, mock_login):
        form = SignupForm(data = {
            'username': 'test_user',
            'password': 'testpassword',
            'confirm_password': 'testpassword',
            'email': 'test_user@test.com'
        })

        request = RequestFactory()
        self.assertTrue(form.is_valid(request))

        user = User.objects.get(username='test_user')
        mock_login.assert_called_once_with(request, user)


    @mock.patch('customers.forms.login')
    @mock.patch('customers.forms.SignupForm.save')
    def test_validation_for_empty_items(self, mock_login, mock_save):
        form = SignupForm(data={})

        self.assertFalse(form.is_valid(RequestFactory()))
        self.assertEqual(len(form.errors['username']), 1)
        self.assertEqual(len(form.errors['password']), 1)

        self.assertFalse(mock_login.called)
        self.assertFalse(mock_save.called)


    @mock.patch('customers.forms.SignupForm.save')
    def test_form_validate_confirm_password(self, mock_save):
        form = SignupForm(data = {
            'username': 'test_user',
            'password': 'testpassword',
            'confirm_password': 'wrongpassword',
            'email': 'test_user@test.com'
        })

        self.assertFalse(form.is_valid(RequestFactory()))
        self.assertEqual(len(form.errors['confirm_password']), 1)
        self.assertFalse(mock_save.called)

    @mock.patch('customers.forms.authenticate', return_value=None)
    def test_form_after_created_an_user_cant_authenticate(self, mock_authenticate):
        form = SignupForm(data = {
            'username': 'test_user',
            'password': 'testpassword',
            'confirm_password': 'testpassword',
            'email': 'test_user@test.com'
        })


        self.assertFalse(form.is_valid(RequestFactory()))
        self.assertTrue(mock_authenticate.called)
        self.assertEqual(len(form.errors['username']), 1)

