from django.test import TestCase
from django.core.urlresolvers import reverse

from django.contrib.auth.models import User
from customers.forms import SignupForm

from django.http import HttpResponse
from unittest import skip

import mock

class SignupViewTest(TestCase):

    def test_view_login_renders_the_right_template(self):
        response = self.client.get(reverse('account:login'))
        self.assertTemplateUsed(response, 'customers/login.html')

    @mock.patch('customers.views.user_login')
    def test_view_if_post_with_login_should_call_user_login(self, mock_user_login):
        mock_user_login.return_value = HttpResponse('hello world!')

        self.client.post(reverse('account:login'), {
                'login': 'whatever'
            })

        self.assertTrue(mock_user_login.called)

    @mock.patch('customers.views.user_signup')
    def test_view_if_post_with_login_should_call_user_create(self, mock_user_signup):
        mock_user_signup.return_value = HttpResponse('hello world!')

        self.client.post(reverse('account:login'), {
                'signup': 'whatever'
            })

        self.assertTrue(mock_user_signup.called)

    def test_view_signup_uses_correct_form(self):
        response = self.client.get(reverse('account:login'))
        self.assertIsInstance(response.context['signup_form'], SignupForm)


    def test_view_signup_valid_should_redirect_to_myaccount(self):
        response = self.client.post(reverse('account:login'), {
                'username': 'test_user',
                'email': 'test@email.com',
                'password': 'testpassword',
                'confirm_password': 'testpassword',
                'signup': 'whatever'
            }, follow=True)

        self.assertEqual(len(response.redirect_chain), 1)
        self.assertRedirects(response, reverse('account:myaccount'), status_code=302, target_status_code=200, fetch_redirect_response=True)


    def test_view_signup_invalid_should_redirect_to_login(self):
        user = User.objects.create_user(username='test_user', password='password', email='test@email.com')
        user.save()

        response = self.client.post(reverse('account:login'), {
                'username': 'test_user',
                'email': 'test@email.com',
                'password': 'testpassword',
                'confirm_password': 'testpassword',
                'signup': 'whatever'
            }, follow=True)

        self.assertTemplateUsed(response, 'customers/login.html')
        self.assertEqual(len(response.context['signup_form'].errors), 1)
        self.assertEqual(len(response.context['signup_form'].errors['username']), 1)


    def test_view_signup_check_password_confirmation(self):
        response = self.client.post(reverse('account:login'), {
                'username': 'test_user',
                'email': 'test@email.com',
                'password': 'testpassword',
                'confirm_password': 'wrongpasword',
                'signup': 'whatever'
            }, follow=True)

        self.assertTemplateUsed(response, 'customers/login.html')
        self.assertEqual(len(response.context['signup_form'].errors), 1)
        self.assertEqual(len(response.context['signup_form'].errors['confirm_password']), 1)

