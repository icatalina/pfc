from django.test import TestCase
from django.core.urlresolvers import reverse

from django.contrib.auth.models import User
from customers.forms import LoginForm

from unittest import skip

import mock

class LoginViewsTest(TestCase):

    def test_view_login_returns_200(self):
        response = self.client.get(reverse('account:login'))
        self.assertEqual(response.status_code, 200)


    def test_view_login_uses_correct_form(self):
        response = self.client.get(reverse('account:login'))
        self.assertIsInstance(response.context['form'], LoginForm)


    def test_view_login_should_redirect_on_valid_form_and_post(self):
        user = User.objects.create_user('testuser', 'test@djangotest.com', 'testpassword')
        user.save()

        response = self.client.post(reverse('account:login'), {
                'username': 'testuser',
                'password': 'testpassword',
                'login': 'whatever'
            }, follow=True)

        self.assertRedirects(response, reverse('account:myaccount'), status_code=302, target_status_code=200, fetch_redirect_response=True)


    @mock.patch('customers.forms.LoginForm.is_valid')
    def test_view_login_should_return_to_login_on_not_valid_form(self, mock_is_valid):
        mock_is_valid.return_value = False

        response = self.client.post(reverse('account:login'), {
                'username': 'test_user',
                'password': 'testpassword',
                'login': 'whatever'
            }, follow=True)

        self.assertEqual(mock_is_valid.call_count, 1)
        self.assertEqual(len(response.redirect_chain), 0)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'customers/login.html')


    @mock.patch('customers.views.logout')
    def test_view_logout_should_log_you_out_when_visiting_logout(self, mock_logout):
        user = User.objects.create_user('testuser', 'test@djangotest.com', 'testpassword')
        user.save()
        logedIn = self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('account:logout'), follow=True)

        self.assertTrue(logedIn)
        self.assertRedirects(response, reverse('home'), status_code=302, target_status_code=200, fetch_redirect_response=True)
        self.assertEqual(mock_logout.call_count, 1)

