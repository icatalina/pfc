from django.test import TestCase
from django.core.urlresolvers import reverse

from django.contrib.auth.models import User
from customers.forms import MyAccountForm

from django.utils.http import urlquote_plus
from unittest import skip

import mock

class MyAccountViewTest(TestCase):
    fixtures = ['users.json']

    def setUp(self):
        self.client.login(username='test_user', password='testpassword')


    def test_view_myaccount_redirect_to_login_when_not_logged_in(self):
        self.client.logout()
        response = self.client.get(reverse('account:myaccount'), follow=True)
        url = reverse('account:login') + '?next=' + urlquote_plus(reverse('account:myaccount'))
        self.assertRedirects(response, url, status_code=302, target_status_code=200, fetch_redirect_response=True)


    def test_should_show_my_account_and_render_the_right_template(self):
        response = self.client.get(reverse('account:myaccount'), follow=True)
        self.assertEqual(len(response.redirect_chain), 0)
        self.assertTemplateUsed(response, 'customers/myaccount.html')


    def test_should_render_the_right_form(self):
        response = self.client.get(reverse('account:myaccount'), follow=True)
        self.assertIsInstance(response.context['myaccount_form'], MyAccountForm)


    def test_should_update_user_on_save(self):
        users = User.objects.count()

        response = self.client.post(reverse('account:myaccount'), {
            'username': 'perico',
            'email': 'perico@perico.com'
        }, follow=True)

        self.assertEqual(users, User.objects.count())
        user = User.objects.get(pk=response.context['user'].id)
        self.assertEqual(user.username, 'perico')
        self.assertEqual(user.email, 'perico@perico.com')


    def test_should_not_update_if_data_is_wrong_but_should_keep_the_data(self):
        users = User.objects.count()

        response = self.client.post(reverse('account:myaccount'), {
            'username': 'perico',
            'email': 'pericoperico.com'
        }, follow=True)

        self.assertEqual(users, User.objects.count())
        user = User.objects.get(pk=response.context['user'].id)
        self.assertNotEqual(user.username, 'perico')
        self.assertNotEqual(user.email, 'pericoperico.com')
        self.assertEqual(response.context['myaccount_form']['username'].value(), 'perico')
        self.assertEqual(response.context['myaccount_form']['email'].value(), 'pericoperico.com')
