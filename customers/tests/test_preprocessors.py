from django.test import TestCase

from customers.preprocessors import basket, quantities
from products.models import Sku
from django.test.client import RequestFactory
from unittest import skip

from decimal import Decimal

class Preprocessors(TestCase):
    fixtures = ['functional']

    def test_should_return_empty_if_basket_not_in_session(self):
        self.assertEqual(basket(self.client), {'basket': { 'count': 0, 'price': 0, 'skus': {}}})

    def test_should_return_the_right_objects_if_session_set(self):
        session = self.client.session
        session['basket'] = {
            '12345': 2,
            '12339': 1
        }
        session.save()

        response = basket(self.client)
        self.assertIn('basket', response)

        bs = response['basket']['skus']
        self.assertEqual(response['basket']['count'], 3)

        self.assertEqual(bs['12345']['quantity'], 2)
        self.assertEqual(bs['12339']['quantity'], 1)

        self.assertIsInstance(bs['12345']['sku'], Sku)
        self.assertIsInstance(bs['12339']['sku'], Sku)

        self.assertEqual(bs['12345']['sku'].sku, '12345')
        self.assertEqual(bs['12339']['sku'].sku, '12339')
        # Checking cart price
        self.assertEqual(str(response['basket']['price']), '60.60')

    def test_should_fail_gracefully_if_object_doesnt_exist_in_the_db(self):
        session = self.client.session
        session['basket'] = {
            '12777': 2
        }
        session.save()

        response = basket(self.client)
        self.assertIn('basket', response)

        bs = response['basket']
        self.assertEqual(bs['count'], 0)

    def test_quantities_should_return_a_range_0_to_settings_qnt(self):
        res = quantities(True)
        self.assertEqual(len(res['quantities']), 9)



