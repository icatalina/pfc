from django.test import TestCase
from django.core.urlresolvers import reverse

from products.models import Sku

from unittest import skip

import mock

class BasketViewsTest(TestCase):
    fixtures = ['functional']

    def test_view_basket_returns_200(self):
        response = self.client.get(reverse('account:basket'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'customers/basket.html')

    def test_view_basket_sets_the_right_element_in_the_context(self):
        session = self.client.session
        session['basket'] = {
            '12345': 2
        }

        session.save()

        response = self.client.get(reverse('account:basket'))

        skus = response.context['basket']['skus']
        self.assertEqual(len(skus), 1)
        self.assertEqual(skus['12345']['quantity'], 2)
        self.assertIsInstance(skus['12345']['sku'], Sku)

    def test_view_basket_sets_multiple_elements_in_the_context(self):
        session = self.client.session
        session['basket'] = {
            '12345': 2,
            '12339': 1
        }

        session.save()

        response = self.client.get(reverse('account:basket'))

        skus = response.context['basket']['skus']
        self.assertEqual(len(skus), 2)
        self.assertEqual(skus['12345']['quantity'], 2)
        self.assertEqual(skus['12339']['quantity'], 1)
        self.assertIsInstance(skus['12345']['sku'], Sku)
        self.assertIsInstance(skus['12339']['sku'], Sku)


    def test_view_basket_fails_gracefully_if_element_doesnt_exist(self):
        session = self.client.session
        session['basket'] = {
            '12344': 2
        }

        session.save()

        response = self.client.get(reverse('account:basket'))
        self.assertEqual(len(response.context['basket']['skus']), 0)
        self.assertEqual(response.context['basket']['count'], 0)


    def test_view_basket_should_include_the_valid_skus(self):
        session = self.client.session
        session['basket'] = {
            '12344': 2,
            '12339': 1
        }

        session.save()

        response = self.client.get(reverse('account:basket'))

        skus = response.context['basket']['skus']
        self.assertEqual(len(skus), 1)
        self.assertEqual(skus['12339']['quantity'], 1)
        self.assertIsInstance(skus['12339']['sku'], Sku)
        self.assertNotIn('12344', skus)

    def test_view_basket_should_not_add_items_with_zero_quantity(self):
        session = self.client.session
        session['basket'] = {
            '12345': 2,
            '12339': 0
        }

        session.save()

        response = self.client.get(reverse('account:basket'))

        skus = response.context['basket']['skus']
        self.assertEqual(len(skus), 1)
        self.assertEqual(skus['12345']['quantity'], 2)
        self.assertIsInstance(skus['12345']['sku'], Sku)
        self.assertNotIn('12339', skus)


    # Same for GET requests

    def test_view_basket_get_should_add_skus_to_session_and_redirect(self):
        session = self.client.session

        response = self.client.get(reverse('account:basket'), {
            'sku': '12345',
            'quantity': 2
        }, follow=True)

        self.assertRedirects(response, reverse('account:basket'), status_code=302, target_status_code=200, fetch_redirect_response=True)

        skus = response.context['basket']['skus']
        self.assertTemplateUsed(response, 'customers/basket.html')
        self.assertEqual(len(skus), 1)
        self.assertEqual(skus['12345']['quantity'], 2)
        self.assertIsInstance(skus['12345']['sku'], Sku)

    def test_view_basket_get_should_increment_count_on_post(self):
        session = self.client.session
        session['basket'] = {
            '12345': 2,
            '12339': 1
        }
        session.save()

        response = self.client.get(reverse('account:basket'), {
            'sku': '12345',
            'quantity': '+2'
        }, follow=True)

        skus = response.context['basket']['skus']
        self.assertEqual(len(skus), 2)
        self.assertEqual(skus['12345']['quantity'], 4)
        self.assertEqual(skus['12339']['quantity'], 1)

    def test_view_basket_get_should_decrement_count_on_post(self):
        session = self.client.session
        session['basket'] = {
            '12345': 2,
            '12339': 1
        }
        session.save()

        response = self.client.get(reverse('account:basket'), {
            'sku': '12345',
            'quantity': '-1'
        }, follow=True)

        skus = response.context['basket']['skus']
        self.assertEqual(len(skus), 2)
        self.assertEqual(skus['12345']['quantity'], 1)
        self.assertEqual(skus['12339']['quantity'], 1)

    def test_view_basket_get_should_delete_the_item_if_count_is_set_to_zero(self):
        session = self.client.session
        session['basket'] = {
            '12345': 2,
            '12339': 1
        }
        session.save()

        response = self.client.get(reverse('account:basket'), {
            'sku': '12345',
            'quantity': 0
        }, follow=True)

        skus = response.context['basket']['skus']
        self.assertEqual(len(skus), 1)
        self.assertNotIn('12345', skus)
        self.assertEqual(skus['12339']['quantity'], 1)


