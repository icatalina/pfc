from django.test import TestCase
from django.contrib.auth.models import User
from customers.forms import MyAccountForm

from unittest import skip

import mock

class MyAccountFormTest(TestCase):
    fixtures = ['users.json']

    def test_given_an_user_it_should_populate_the_data_in_the_form_and_keep_passwords_empty(self):
        user = User.objects.first()
        form = MyAccountForm(instance=user)

        self.assertTrue(len(form.fields), 4)
        self.assertIn('username', form.fields)
        self.assertIn('email', form.fields)
        self.assertIn('password', form.fields)
        self.assertIn('confirm_password', form.fields)

        self.assertIn('value="%s"' % (user.username), form['username'].__str__())
        self.assertIn('value="%s"' % (user.email), form['email'].__str__())
        self.assertNotIn('value="', form['password'].__str__())
        self.assertNotIn('value="', form['confirm_password'].__str__())


    def test_should_let_you_update_the_form_but_keep_the_password(self):
        user = User.objects.first()
        password = user.password
        id = user.id

        form = MyAccountForm({
            'username': 'perico',
            'email': 'email@email.com'
        }, instance=user)


        self.assertTrue(form.is_valid())
        savedUser = form.save()


        user = User.objects.first()

        self.assertEqual(user, savedUser)
        self.assertEqual(user.username, 'perico')
        self.assertEqual(user.email, 'email@email.com')
        self.assertEqual(user.id, id)
        self.assertEqual(user.password, password)


    def test_should_update_the_password_and_the_rest_of_the_information(self):
        user = User.objects.first()
        password = user.password

        form = MyAccountForm({
            'username': 'perico',
            'email': 'email@email.com',
            'password': 'test_password_2',
            'confirm_password': 'test_password_2'
        }, instance=user)

        self.assertTrue(form.is_valid())
        savedUser = form.save()

        user = User.objects.first()
        self.assertEqual(user, savedUser)
        self.assertNotEqual(user.password, password)
        self.assertNotEqual(user.password, 'test_password_2')


    def test_should_fail_when_wrong_password_confirmation(self):
        user = User.objects.first()
        password = user.password

        form = MyAccountForm({
            'username': 'perico',
            'email': 'email@email.com',
            'password': 'test_password_2',
            'confirm_password': 'test_password'
        }, instance=user)

        self.assertFalse(form.is_valid())
        self.assertIn('confirm_password', form.errors)
