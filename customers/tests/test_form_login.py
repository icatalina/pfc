from django.test import TestCase

from customers.forms import LoginForm

from django.test.client import RequestFactory
from unittest import skip

import mock

class LoginFormTest(TestCase):
    fixtures = ['users.json']

    def test_form_right_items_are_included(self):
        form = LoginForm()
        self.assertIn('name="username"', form.as_p())
        self.assertIn('name="password"', form.as_p())


    @mock.patch('customers.forms.login')
    def test_form_logs_in(self, mock_login):
        request = RequestFactory()
        request = request.get('/account/login')
        request.session = self.client.session

        form = LoginForm(data = {
            'username': 'test_user',
            'password': 'testpassword'
        })

        self.assertTrue(form.is_valid(request))
        self.assertTrue(mock_login.called)


    def test_form_missing_user_and_password_throw_2_errors(self):
        request = RequestFactory()
        request = request.get('/account/login')
        request.session = self.client.session

        form = LoginForm(data = {})

        self.assertFalse(form.is_valid(request))
        self.assertEqual(len(form.errors['username']), 1)
        self.assertEqual(len(form.errors['password']), 1)


    def test_form_wrong_username_or_password_throws_error(self):
        request = RequestFactory()
        request = request.get('/account/login')
        request.session = self.client.session

        form = LoginForm(data = {
            'username': 'test_user',
            'password': 'wrong_password'
        })

        self.assertFalse(form.is_valid(request))
        self.assertEqual(len(form._errors), 1)

        form = LoginForm(data = {
            'username': 'wrong_username',
            'password': 'testpassword'
        })

        self.assertFalse(form.is_valid(request))
        self.assertEqual(len(form.errors), 1)
