from django.test import TestCase
from django.contrib.auth.models import User
from customers.forms import CheckoutForm

from django.test.client import RequestFactory
from unittest import skip
import mock

class CheckoutFormTest(TestCase):
    fixtures = ['unittest']

    def test_form_save_cart(self):
        user = User.objects.get(pk=100)

        form = CheckoutForm(data = {
           'address1': 'My Street',
           'address2': 'My second Street',
           'city': 'My City',
           'phone': 'My Phone'
        })

        skus = { '12345': 2, '12339': 1 }

        self.assertEqual(form.is_valid(), True)
        form.save(user, skus)
