from django.test import TestCase
from customers.models import CartOrder
from django.contrib.auth.models import User

from django.db.utils import IntegrityError

from django.test.client import RequestFactory
from unittest import skip
import mock

class Order(TestCase):
    fixtures = ['unittest']

    def test_can_create_an_order(self):
        user = User.objects.get(pk=100)

        sku = CartOrder.objects.create(
                skus={ '12339': '1', '12345': '3' },
                customer=user,
                address1='100 Gosbrook Road',
                address2='Elizabeth House',
                city='Reading',
                phone='07708080808'
                )

    def test_should_fail_without_skus(self):
        user = User.objects.get(pk=100)

        with self.assertRaises(IntegrityError):
            CartOrder.objects.create(
                    customer=user,
                    address1='100 Gosbrook Road',
                    address2='Elizabeth House',
                    city='Reading',
                    phone='07708080808'
                    )

    def test_should_fail_without_address1(self):
        user = User.objects.get(pk=100)

        with self.assertRaises(IntegrityError):
            CartOrder.objects.create(
                    skus={ '12339': '1', '12345': '3' },
                    customer=user,
                    address2='Elizabeth House',
                    city='Reading',
                    phone='07708080808'
                    )

    def test_should_fail_without_user(self):
        with self.assertRaises(IntegrityError):
            CartOrder.objects.create(
                    skus={ '12339': '1', '12345': '3' },
                    address1='100 Gosbrook Road',
                    address2='Elizabeth House',
                    city='Reading',
                    phone='07708080808'
                    )
