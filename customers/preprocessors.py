from products.models import Sku
from django.db import connection

def basket(request):
    exports = {
        'count': 0,
        'price': 0,
        'skus': {}
    }

    if 'basket' in request.session:
        session_basket = request.session['basket']
        elements = Sku.objects.filter(pk__in=(session_basket.keys()))
        bs = {}
        for elem in elements:
            if session_basket[elem.sku] > 0:
                quantity = session_basket[elem.sku]
                exports['count'] = exports['count'] + quantity
                bs[elem.sku] = {
                    'quantity': quantity,
                    'price': elem.price * quantity,
                    'sku': elem
                }
                exports['price'] = exports['price'] + (quantity * elem.price)

        exports['skus'] = bs

    return { 'basket': exports }

def quantities(request):
    return { 'quantities': range(1, 10) }
