from django.shortcuts import redirect, render
from .forms import LoginForm, SignupForm, MyAccountForm

import sys
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
import re

def user_signup(request):
    if request.method == 'POST':
        signup_form = SignupForm(request.POST)

        if signup_form.is_valid(request):
            return redirect('account:myaccount')

    form = LoginForm()

    context = {'form': form, 'signup_form': signup_form}
    return render(request, 'customers/login.html', context)

def user_login(request):

    if request.method == 'POST':
        form = LoginForm(request.POST)

        if form.is_valid(request):
            return redirect('account:myaccount')

    signup_form = SignupForm()

    context = {'form': form, 'signup_form': signup_form}
    return render(request, 'customers/login.html', context)

def user_login_signup(request):

    if 'login' in request.POST:
        return user_login(request)
    elif 'signup' in request.POST:
        return user_signup(request)
    else:
        form = LoginForm()
        signup_form = SignupForm()

    context = {'form': form, 'signup_form': signup_form}
    return render(request, 'customers/login.html', context)

@login_required
def user_profile(request):
    myaccount_form = MyAccountForm(data=request.POST or None, instance=request.user)

    if myaccount_form.is_valid():
        myaccount_form.save()

    context = { 'myaccount_form': myaccount_form }
    return render(request, 'customers/myaccount.html', context)

@login_required
def user_logout(request):
    logout(request)
    return redirect('home')

def basket(request):
    sku = request.POST.get('sku', request.GET.get('sku', False))
    qnt = request.POST.get('quantity', request.GET.get('quantity', False))

    if sku and qnt:
        basket = request.session.get('basket', default={})

        if re.match('^[+\-]?\d+$', qnt):
            qnt = int(qnt)
            if sku in basket and qnt != 0:
                basket[sku] = basket[sku] + qnt
            else:
                basket[sku] = qnt

        request.session['basket'] = basket
        return redirect('account:basket')

    return render(request, 'customers/basket.html')
