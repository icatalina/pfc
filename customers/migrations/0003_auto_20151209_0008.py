# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields.hstore


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0002_cartorder'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cartorder',
            name='skus',
            field=django.contrib.postgres.fields.hstore.HStoreField(),
        ),
    ]
