# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields.hstore
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('customers', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CartOrder',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('skus', django.contrib.postgres.fields.hstore.HStoreField(blank=True, null=True)),
                ('address1', models.CharField(max_length=255)),
                ('address2', models.CharField(max_length=255, null=True)),
                ('city', models.CharField(max_length=255)),
                ('phone', models.CharField(max_length=25)),
                ('customer', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
