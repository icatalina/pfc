# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0003_auto_20151209_0008'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cartorder',
            name='address1',
            field=models.CharField(max_length=255, default=None),
        ),
    ]
