from django.conf.urls import include, url

from . import views

urlpatterns = [
    # Examples:
    # url(r'^$', 'sikker.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^login/', views.user_login_signup, name='login'),
    url(r'^logout/', views.user_logout, name='logout'),
    url(r'^myaccount/', views.user_profile, name='myaccount'),
    url(r'^basket', views.basket, name='basket')
]
