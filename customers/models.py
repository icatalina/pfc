from django.db import models
from django.contrib.auth.models import User
from django.contrib.postgres.fields import HStoreField

class CartOrder(models.Model):
    customer = models.ForeignKey(User)
    skus = HStoreField(db_index=False)
    address1 = models.CharField(max_length=255, blank=False, default=None)
    address2 = models.CharField(max_length=255, null=True)
    city = models.CharField(max_length=255)
    phone = models.CharField(max_length=25)


