from django import forms
import django.contrib.auth
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from customers.models import CartOrder
from django.utils.translation import ugettext as _

class LoginForm(forms.Form):
    username = forms.CharField(label=_('Username'), max_length=255, required=True)
    password = forms.CharField(label=_('Password'), widget=forms.PasswordInput(), required=True)

    def is_valid(self, request):
        valid = super(LoginForm, self).is_valid()

        if valid:

            user = authenticate(username=self.cleaned_data['username'], password=self.cleaned_data['password'])

            if user is not None and user.is_active:
                login(request, user)
                return True

            else:
                self.errors['username'] = ['Password is invalid', ]
                return False

        else:
            return False

class SignupForm(forms.ModelForm):

    confirm_password = forms.CharField(label=_('Confirm password'),widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ['username', 'email', 'password']
        widgets = {
            'password': forms.PasswordInput(),
        }

    def is_valid(self, request):
        valid = super(SignupForm, self).is_valid()

        if valid and self.cleaned_data['confirm_password'] != self.cleaned_data['password']:
            self.errors['confirm_password'] = ['Both passwords don\'t match', ]
            valid = False

        if valid:
            user = self.save()
            user.set_password(self.cleaned_data['password'])
            user.save()

            user = authenticate(username=self.cleaned_data['username'], password=self.cleaned_data['password'])

            if user is not None and user.is_active:
                login(request, user)
                return True

            else:
                self.errors['username'] = ['Password is invalid', ]
                return False

        else:
            return False


class MyAccountForm(forms.ModelForm):

    password = forms.CharField(required=False, widget=forms.PasswordInput)
    confirm_password = forms.CharField(label=_('Confirm password'), required=False,widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ['username', 'email', 'password']

    def is_valid(self):
        valid = super(MyAccountForm, self).is_valid()

        if valid and self.cleaned_data['confirm_password'] != self.cleaned_data['password']:
            self.errors['confirm_password'] = ['Both passwords don\'t match', ]
            valid = False

        return valid

    def save(self):
        user = super(MyAccountForm, self).save()
        password = self.cleaned_data['password']

        if password:
            user.set_password(password)

        user.save()
        return user

class CheckoutForm(forms.ModelForm):

    class Meta:
        model = CartOrder
        fields = ['address1', 'address2', 'city', 'phone']

    def save(self, user, skus):
        if user and skus:
            for key in skus.keys():
                skus[key] = str(skus[key])

            form = super(CheckoutForm, self).save(commit=False)
            form.customer = user
            form.skus = skus
            form.save()




