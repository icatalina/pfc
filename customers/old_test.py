from django.test import TestCase
from customers import views
from django.core.urlresolvers import reverse

from django.contrib.auth.models import User
from customers.forms import LoginForm, SignupForm, MyAccountForm

from django.http import HttpResponse
from django.test.client import RequestFactory
from django.utils.http import urlquote_plus
from unittest import skip

import mock

class LoginSignUpViewTest(TestCase):

    def test_view_login_renders_the_right_template(self):
        response = self.client.get(reverse('account:login'))
        self.assertTemplateUsed(response, 'customers/login.html')

    @mock.patch('customers.views.user_login')
    def test_view_if_post_with_login_should_call_user_login(self, mock_user_login):
        mock_user_login.return_value = HttpResponse('hello world!')

        self.client.post(reverse('account:login'), {
                'login': 'whatever'
            })

        self.assertTrue(mock_user_login.called)

    @mock.patch('customers.views.user_signup')
    def test_view_if_post_with_login_should_call_user_create(self, mock_user_signup):
        mock_user_signup.return_value = HttpResponse('hello world!')

        self.client.post(reverse('account:login'), {
                'signup': 'whatever'
            })

        self.assertTrue(mock_user_signup.called)

class LoginViewsTest(TestCase):

    def test_view_login_returns_200(self):
        response = self.client.get(reverse('account:login'))
        self.assertEqual(response.status_code, 200)


    def test_view_login_uses_correct_form(self):
        response = self.client.get(reverse('account:login'))
        self.assertIsInstance(response.context['form'], LoginForm)


    def test_view_login_should_redirect_on_valid_form_and_post(self):
        user = User.objects.create_user('testuser', 'test@djangotest.com', 'testpassword')
        user.save()

        response = self.client.post(reverse('account:login'), {
                'username': 'testuser',
                'password': 'testpassword',
                'login': 'whatever'
            }, follow=True)

        self.assertRedirects(response, reverse('account:myaccount'), status_code=302, target_status_code=200, fetch_redirect_response=True)


    @mock.patch('customers.forms.LoginForm.is_valid')
    def test_view_login_should_return_to_login_on_not_valid_form(self, mock_is_valid):
        mock_is_valid.return_value = False

        response = self.client.post(reverse('account:login'), {
                'username': 'test_user',
                'password': 'testpassword',
                'login': 'whatever'
            }, follow=True)

        self.assertEqual(mock_is_valid.call_count, 1)
        self.assertEqual(len(response.redirect_chain), 0)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'customers/login.html')


    @mock.patch('customers.views.logout')
    def test_view_logout_should_log_you_out_when_visiting_logout(self, mock_logout):
        user = User.objects.create_user('testuser', 'test@djangotest.com', 'testpassword')
        user.save()
        logedIn = self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('account:logout'), follow=True)

        self.assertTrue(logedIn)
        self.assertRedirects(response, reverse('home'), status_code=302, target_status_code=200, fetch_redirect_response=True)
        self.assertEqual(mock_logout.call_count, 1)



class SignupViewTest(TestCase):

    def test_view_signup_uses_correct_form(self):
        response = self.client.get(reverse('account:login'))
        self.assertIsInstance(response.context['signup_form'], SignupForm)


    def test_view_signup_valid_should_redirect_to_myaccount(self):
        response = self.client.post(reverse('account:login'), {
                'username': 'test_user',
                'email': 'test@email.com',
                'password': 'testpassword',
                'confirm_password': 'testpassword',
                'signup': 'whatever'
            }, follow=True)

        self.assertEqual(len(response.redirect_chain), 1)
        self.assertRedirects(response, reverse('account:myaccount'), status_code=302, target_status_code=200, fetch_redirect_response=True)


    def test_view_signup_invalid_should_redirect_to_login(self):
        user = User.objects.create_user(username='test_user', password='password', email='test@email.com')
        user.save()

        response = self.client.post(reverse('account:login'), {
                'username': 'test_user',
                'email': 'test@email.com',
                'password': 'testpassword',
                'confirm_password': 'testpassword',
                'signup': 'whatever'
            }, follow=True)

        self.assertTemplateUsed(response, 'customers/login.html')
        self.assertEqual(len(response.context['signup_form'].errors), 1)
        self.assertEqual(len(response.context['signup_form'].errors['username']), 1)


    def test_view_signup_check_password_confirmation(self):
        response = self.client.post(reverse('account:login'), {
                'username': 'test_user',
                'email': 'test@email.com',
                'password': 'testpassword',
                'confirm_password': 'wrongpasword',
                'signup': 'whatever'
            }, follow=True)

        self.assertTemplateUsed(response, 'customers/login.html')
        self.assertEqual(len(response.context['signup_form'].errors), 1)
        self.assertEqual(len(response.context['signup_form'].errors['confirm_password']), 1)

class MyAccountViewTest(TestCase):
    fixtures = ['users.json']

    def setUp(self):
        self.client.login(username='test_user', password='testpassword')


    def test_view_myaccount_redirect_to_login_when_not_logged_in(self):
        self.client.logout()
        response = self.client.get(reverse('account:myaccount'), follow=True)
        url = reverse('account:login') + '?next=' + urlquote_plus(reverse('account:myaccount'))
        self.assertRedirects(response, url, status_code=302, target_status_code=200, fetch_redirect_response=True)


    def test_should_show_my_account_and_render_the_right_template(self):
        response = self.client.get(reverse('account:myaccount'), follow=True)
        self.assertEqual(len(response.redirect_chain), 0)
        self.assertTemplateUsed(response, 'customers/myaccount.html')

    def test_should_render_the_right_form(self):
        response = self.client.get(reverse('account:myaccount'), follow=True)
        self.assertIsInstance(response.context['myaccount_form'], MyAccountForm)

    def test_should_update_user_on_save(self):
        users = User.objects.count()

        response = self.client.post(reverse('account:myaccount'), {
            'username': 'perico',
            'email': 'perico@perico.com'
        }, follow=True)

        self.assertEqual(users, User.objects.count())
        user = User.objects.get(pk=response.context['user'].id)
        self.assertEqual(user.username, 'perico')
        self.assertEqual(user.email, 'perico@perico.com')


    def test_should_not_update_if_data_is_wrong_but_should_keep_the_data(self):
        users = User.objects.count()

        response = self.client.post(reverse('account:myaccount'), {
            'username': 'perico',
            'email': 'pericoperico.com'
        }, follow=True)

        self.assertEqual(users, User.objects.count())
        user = User.objects.get(pk=response.context['user'].id)
        self.assertNotEqual(user.username, 'perico')
        self.assertNotEqual(user.email, 'pericoperico.com')
        self.assertEqual(response.context['myaccount_form']['username'].value(), 'perico')
        self.assertEqual(response.context['myaccount_form']['email'].value(), 'pericoperico.com')


class LoginFormTest(TestCase):
    fixtures = ['users.json']

    def test_form_right_items_are_included(self):
        form = LoginForm()
        self.assertIn('name="username"', form.as_p())
        self.assertIn('name="password"', form.as_p())


    @mock.patch('customers.forms.login')
    def test_form_logs_in(self, mock_login):
        request = RequestFactory()
        request = request.get('/account/login')
        request.session = self.client.session

        form = LoginForm(data = {
            'username': 'test_user',
            'password': 'testpassword'
        })

        self.assertTrue(form.is_valid(request))
        self.assertTrue(mock_login.called)


    def test_form_missing_user_and_password_throw_2_errors(self):
        request = RequestFactory()
        request = request.get('/account/login')
        request.session = self.client.session

        form = LoginForm(data = {})

        self.assertFalse(form.is_valid(request))
        self.assertEqual(len(form.errors['username']), 1)
        self.assertEqual(len(form.errors['password']), 1)


    def test_form_wrong_username_or_password_throws_error(self):
        request = RequestFactory()
        request = request.get('/account/login')
        request.session = self.client.session

        form = LoginForm(data = {
            'username': 'test_user',
            'password': 'wrong_password'
        })

        self.assertFalse(form.is_valid(request))
        self.assertEqual(len(form._errors), 1)

        form = LoginForm(data = {
            'username': 'wrong_username',
            'password': 'testpassword'
        })

        self.assertFalse(form.is_valid(request))
        self.assertEqual(len(form.errors), 1)

class SignUpFormTest(TestCase):

    @mock.patch('customers.forms.login')
    def test_form_create_user_and_login(self, mock_login):
        form = SignupForm(data = {
            'username': 'testuser',
            'password': 'testpassword',
            'confirm_password': 'testpassword',
            'email': 'user@email.com'
        })

        self.assertEqual(form.is_valid(RequestFactory()), True)
        self.assertTrue(mock_login.called)


    def test_form_no_twins(self):
        user = User.objects.create_user('test_user', 'test_user@test.com', 'testpassword')
        user.save()

        form = SignupForm(data = {
            'username': 'test_user',
            'password': 'testpassword',
            'confirm_password': 'testpassword',
            'email': 'test_user@test.com'
        })

        self.assertFalse(form.is_valid(RequestFactory()))
        self.assertTrue(len(form.errors), 1)
        self.assertTrue(len(form.errors['username']), 1)


    @mock.patch('customers.forms.login')
    def test_form_save(self, mock_login):
        form = SignupForm(data = {
            'username': 'test_user',
            'password': 'testpassword',
            'confirm_password': 'testpassword',
            'email': 'test_user@test.com'
        })

        request = RequestFactory()
        self.assertTrue(form.is_valid(request))

        user = User.objects.get(username='test_user')
        mock_login.assert_called_once_with(request, user)


    @mock.patch('customers.forms.login')
    @mock.patch('customers.forms.SignupForm.save')
    def test_validation_for_empty_items(self, mock_login, mock_save):
        form = SignupForm(data={})

        self.assertFalse(form.is_valid(RequestFactory()))
        self.assertEqual(len(form.errors['username']), 1)
        self.assertEqual(len(form.errors['password']), 1)

        self.assertFalse(mock_login.called)
        self.assertFalse(mock_save.called)


    @mock.patch('customers.forms.SignupForm.save')
    def test_form_validate_confirm_password(self, mock_save):
        form = SignupForm(data = {
            'username': 'test_user',
            'password': 'testpassword',
            'confirm_password': 'wrongpassword',
            'email': 'test_user@test.com'
        })

        self.assertFalse(form.is_valid(RequestFactory()))
        self.assertEqual(len(form.errors['confirm_password']), 1)
        self.assertFalse(mock_save.called)

    @mock.patch('customers.forms.authenticate', return_value=None)
    def test_form_after_created_an_user_cant_authenticate(self, mock_authenticate):
        form = SignupForm(data = {
            'username': 'test_user',
            'password': 'testpassword',
            'confirm_password': 'testpassword',
            'email': 'test_user@test.com'
        })


        self.assertFalse(form.is_valid(RequestFactory()))
        self.assertTrue(mock_authenticate.called)
        self.assertEqual(len(form.errors['username']), 1)

class MyAccountFormTest(TestCase):
    fixtures = ['users.json']

    def test_given_an_user_it_should_populate_the_data_in_the_form_and_keep_passwords_empty(self):
        user = User.objects.first()
        form = MyAccountForm(instance=user)

        self.assertTrue(len(form.fields), 4)
        self.assertIn('username', form.fields)
        self.assertIn('email', form.fields)
        self.assertIn('password', form.fields)
        self.assertIn('confirm_password', form.fields)

        self.assertIn('value="%s"' % (user.username), form['username'].__str__())
        self.assertIn('value="%s"' % (user.email), form['email'].__str__())
        self.assertNotIn('value="', form['password'].__str__())
        self.assertNotIn('value="', form['confirm_password'].__str__())


    def test_should_let_you_update_the_form_but_keep_the_password(self):
        user = User.objects.first()
        password = user.password
        id = user.id

        form = MyAccountForm({
            'username': 'perico',
            'email': 'email@email.com'
        }, instance=user)


        self.assertTrue(form.is_valid())
        savedUser = form.save()


        user = User.objects.first()

        self.assertEqual(user, savedUser)
        self.assertEqual(user.username, 'perico')
        self.assertEqual(user.email, 'email@email.com')
        self.assertEqual(user.id, id)
        self.assertEqual(user.password, password)


    def test_should_update_the_password_and_the_rest_of_the_information(self):
        user = User.objects.first()
        password = user.password

        form = MyAccountForm({
            'username': 'perico',
            'email': 'email@email.com',
            'password': 'test_password_2',
            'confirm_password': 'test_password_2'
        }, instance=user)

        self.assertTrue(form.is_valid())
        savedUser = form.save()

        user = User.objects.first()
        self.assertEqual(user, savedUser)
        self.assertNotEqual(user.password, password)
        self.assertNotEqual(user.password, 'test_password_2')


    def test_should_fail_when_wrong_password_confirmation(self):
        user = User.objects.first()
        password = user.password

        form = MyAccountForm({
            'username': 'perico',
            'email': 'email@email.com',
            'password': 'test_password_2',
            'confirm_password': 'test_password'
        }, instance=user)

        self.assertFalse(form.is_valid())
        self.assertIn('confirm_password', form.errors)

